import 'dart:io';

void main(List<String> args) {
  //wereWolf();
  //Kotak(4, 8);
  //MembuatTangga(7, 7);
  loopingmenggunakanfor();
  //looping();
  //greeting();
  //schedule();
}

wereWolf() {
  String? nama, peran;
  String peran1 = "Penyihir";
  String peran2 = 'Guard';
  String peran3 = 'Werewolf';
  stdout.write('Masukan nama kamu: ');
  nama = stdin.readLineSync();
  stdout.write('Masukan peran kamu: ');
  peran = stdin.readLineSync();
  if (nama == "" && peran == "") {
    print('masukan nama dan peran');
  } else {
    if (nama == 'Jane' && peran == peran1) {
      print("Selamat datang di Dunia Werewolf, Jane");
      print(
          "Halo Penyihir Jane, kamu dapat melihat siapa yang menjadi werewolf!");
    } else if (nama == 'Jenita' && peran == peran2) {
      print("Selamat datang di Dunia Werewolf, Jenita");
      print(
          "Halo Guard Jenita, kamu dapat melihat siapa yang menjadi werewolf!");
    } else if (nama == 'Junaedi' && peran == peran3) {
      print("Selamat datang di Dunia Werewolf, Junaedi");
      print(
          "Halo Werewolf Junaedi, kamu dapat melihat siapa yang menjadi werewolf!");
    } else {
      print("Selamat datang di Dunia Werewolf, $nama");
      print(
          "Halo $peran $nama, kamu dapat melihat siapa yang menjadi werewolf!");
    }
  }
}

Kotak(int a, int b) {
  for (int i = 0; i < a; i++) {
    for (int j = 0; j < b; j++) {
      stdout.write('# ');
    }
    print(' ');
  }
}

MembuatTangga(int h, int b) {
  for (int i = 0; i < h; i++) {
    for (int j = 0; j <= i; j++) {
      stdout.write('#');
    }
    print('');
  }
}

loopingmenggunakanfor() {
  for (int i = 1; i <= 20; i++) {
    if (i % 2 == 1) {
      print("$i - Santai");
    }
    if (i % 2 == 0) {
      print("$i - Berkualitas");
    }
    if (i % 3 == 0) {
      print("$i - I Love Coding");
    }
  }
}

looping() {
  stdout.write('Masukan judul: ');
  String? judul = stdin.readLineSync();
  if (judul == 'LOOPING PERTAMA') {
    for (int i = 0; i < 10; i++) {
      print(i);
    }
  } else if (judul == 'LOOPING KEDUA') {
    for (int i = 20; i > 0; i--) {
      print(i);
    }
  } else if (judul == 'LOOPING KETIGA') {
    int i = 0;
    while (i < 10) {
      print(i);
      i++;
    }
  } else if (judul == 'LOOPING KEEMPAT') {
    int i = 20;
    do {
      print(i);
      i--;
    } while (i > 0);
  } else {
    print('Judul tidak diketahui');
  }
}

greeting() {
  stdout.write('Enter your name: ');
  String? name = stdin.readLineSync();
  print('Hello, ${name}');
}

schedule() {
  // implementation switch case with i/o
  String? hari, jam;

  stdout.write('Masukan hari: ');
  hari = stdin.readLineSync();
  stdout.write('Masukan jam: ');
  jam = stdin.readLineSync();
  if (hari == null && jam == null) {
    print('masukan hari dan jam');
  } else {
    switch (hari) {
      case 'senin':
        switch (jam) {
          case 'pagi':
            print('Makan siang');
            break;
          case 'siang':
            print('Makan malam');
            break;
          default:
            print('Hari dan jam tidak diketahui');
        }
        break;
      case 'selasa':
        switch (jam) {
          case 'pagi':
            print('Makan siang');
            break;
          case 'siang':
            print('Makan malam');
            break;
          default:
            print('Hari dan jam tidak diketahui');
        }
        break;
      case 'rabu':
        switch (jam) {
          case 'pagi':
            print('Makan siang');
            break;
          case 'siang':
            print('Makan malam');
            break;
          default:
            print('Hari dan jam tidak diketahui');
        }
        break;
      case 'kamis':
        switch (jam) {
          case 'pagi':
            print('Makan siang');
            break;
          case 'siang':
            print('Makan malam');
            break;
          default:
            print('Hari dan jam tidak diketahui');
        }
        break;
      case 'jumat':
        switch (jam) {
          case 'pagi':
            print('Makan siang');
            break;
          case 'siang':
            print('Makan malam');
            break;
          default:
            print('Hari dan jam tidur');
        }
        break;
    }
  }
}
