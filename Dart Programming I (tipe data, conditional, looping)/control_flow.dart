void main() {
// percabangan if
  int totalBelanja = 10000;
  if (totalBelanja >= 10000) {
    print("Selamat kamu mendapatkan diskon!");
  }
//if/ELse
  String password = "adam dwi j";
  if (password == "adam dwi j") {
    print("halo");
  } else {
    print("siapa?");
  }
//IF/ELSE IF/ELSE
  int nilai = 80;
  String grade;
  if (nilai >= 90)
    grade = "A+";
  else if (nilai >= 80)
    grade = "A";
  else if (nilai >= 70)
    grade = "B+";
  else if (nilai >= 60)
    grade = "B";
  else if (nilai >= 50)
    grade = "C+";
  else if (nilai >= 40)
    grade = "C";
  else if (nilai >= 30)
    grade = "D";
  else if (nilai >= 20)
    grade = "E";
  else
    grade = "F";

  print(grade);

  //switch
  String hari = "senin";
  String day;
  switch (hari) {
    case "senin":
      {
        day = "senin";
        break;
      }
    case "selasa":
      {
        day = "selasa";
        break;
      }
    case "rabu":
      {
        day = "rabu";
        break;
      }
    case "kamis":
      {
        day = "kamis";
        break;
      }
    case "jumat":
      {
        day = "Jum'at berkah";
        break;
      }
    case "sabtu":
      {
        day = "sabtu";
        break;
      }
    case "minggu":
      {
        day = "minggu";
        break;
      }
    default:
      {
        day = "Hari yang anda masukan salah!";
      }
  }
  print(day);

  //tenary option
  int h = 10;
  int j = 8;

  int maksimum = h > j ? h : j;
  print(maksimum);
}
