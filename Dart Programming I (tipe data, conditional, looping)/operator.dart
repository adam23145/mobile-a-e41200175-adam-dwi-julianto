void main() {
  //Operator aritmatika
  int a = 3;
  int b = 5;

  print(a + b);
  print(a - b);
  print(a * b);
  print(a / b);
  print(b % a);

  //Operator Assignment (=)
  var angka;
  angka = 10;
  print(angka);

  //Equal Operator (==)
  var angka2 = 100;
  print(angka2 == 100); // true
  print(angka2 == 20); // false

  //Not Equal ( != )
  var sifat = "rajin";
  print(sifat != "malas"); // true
  print(sifat != "bandel"); //true

  var angka3 = 8;
  print(angka3 == "8"); // true, padahal "8" adalah string.
  print(angka3 == "8"); // false, karena tipe data nya berbeda
  print(angka3 == 8); // true

  //Kurang dari & Lebih Dari ( <, >, <=, >=)
  var number = 17;
  print(number < 20); // true
  print(number > 17); // false
  print(number >= 17); // true, karena terdapat sama dengan
  print(number <= 20); // true

  //OR ( || )
  print(true || true); // true
  print(true || false); // true
  print(true || false || false); // true
  print(false || false); // false

  //AND ( && )
  print(true && true); // true
  print(true && false); // false
  print(false && false); // false
  print(false && true && true); // false
  print(true && true && true); // true
}
