void main() {
  num number = 2;
  String string = 'adam';
  bool hu = true && false;
  List<int> list = [23, 24, 35];
  Map<String, String> kota = {
    "jkt": "Jakarta",
    "bdg": "Bandung",
    "sby": "Surabaya"
  };

//number : tipe data angka
  print(number);
//String : tipe data berupa text atau kumpulan karakter
  print(String);
//Boolean: tipe data dengan nilai true atau false
  print(hu);
//List & maps : daftar tipe data untuk merepresentasikan sekumpulan object
  print(list[0]);
  print(list[1]);
  print(list[2]);

  print(kota["jkt"]);
  print(kota["bdg"]);
  print(kota["sby"]);
}
