import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';

void main() {
  runApp(new MaterialApp(
    debugShowCheckedModeBanner: false,
    home: new MyApp(),
  ));
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  final List<String> gambar = [
    "img_saly10.png",
    "img_saly20.png",
    "img_saly24.png",
    "img_saly25.png"
  ];

  static const Map<String, Color> colors = {
    'img_saly10': Color(0xFF2DB569),
    'img_saly20': Color(0xFF386B8),
    'img_saly24': Color(0xFF45CAF5),
    'img_saly25': Color(0xFFB19ECB),
  };
  @override
  Widget build(BuildContext context) {
    timeDilation = 5.0;
    return Scaffold(
      body: new Container(
        decoration: new BoxDecoration(
          gradient: new LinearGradient(
              begin: FractionalOffset.topCenter,
              end: FractionalOffset.bottomCenter,
              colors: [
                Colors.white,
                Colors.purpleAccent,
                Colors.deepPurple,
              ]),
        ),
        child: PageView.builder(
            controller: PageController(viewportFraction: 0.8),
            itemCount: gambar.length,
            itemBuilder: (BuildContext context, int i) {
              return Padding(
                padding: EdgeInsets.symmetric(horizontal: 5.0, vertical: 50.0),
                child: Material(
                  elevation: 5.0,
                  child: Stack(
                    fit: StackFit.expand,
                    children: [
                      new Hero(
                          tag: gambar[i],
                          child: Material(
                            child: new InkWell(
                              child: Flexible(
                                flex: 1,
                                child: Container(
                                  color: colors.values.elementAt(i),
                                  child: Image.asset(
                                    "assets/images/${gambar[i]}",
                                    fit: BoxFit.cover,
                                  ),
                                ),
                              ),
                              onTap: () => Navigator.of(context).push(
                                  MaterialPageRoute(
                                      builder: (BuildContext context) =>
                                          Halamandua(
                                            gambar: gambar[i],
                                            colors: colors.values.elementAt(i),
                                          ))),
                            ),
                          ))
                    ],
                  ),
                ),
              );
            }),
      ),
    );
  }
}

class Halamandua extends StatefulWidget {
  const Halamandua({Key? key, required this.gambar, required this.colors})
      : super(key: key);
  final String gambar;
  final Color colors;

  @override
  State<Halamandua> createState() => _HalamanduaState();
}

class _HalamanduaState extends State<Halamandua> {
  Color warna = Colors.grey;

  void _pilihanya(Pilihan pilihan){
    setState(() {
      warna = pilihan.warna;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("BT21"), 
      backgroundColor: Colors.purpleAccent,
      actions: <Widget>[
        new PopupMenuButton<Pilihan>(onSelected: _pilihanya,
        itemBuilder: (BuildContext context){
          return listPilihan.map((Pilihan x){
            return new PopupMenuItem<Pilihan>(
              child: new Text(x.teks),
              value: x,
            );
          }).toList();
        })
      ],
      ),
      body: new Stack(
        children: [
          Container(
              decoration: BoxDecoration(
            gradient: RadialGradient(
                center: Alignment.center,
                colors: [Colors.purple, Colors.white, warna]),
          )),
          new Center(
              child: new Hero(
                  tag: widget.gambar,
                  child: ClipOval(
                    child: new SizedBox(
                      width: 200.0,
                      height: 200.0,
                      child: new Material(
                        child: InkWell(
                          onTap: () => Navigator.of(context).pop(),
                          child: Flexible(
                              flex: 1,
                              child: Container(
                                  color: widget.colors,
                                  child: new Image.asset(
                                    "assets/images/${widget.gambar}",
                                    fit: BoxFit.cover,
                                  ))),
                        ),
                      ),
                    ),
                  )))
        ],
      ),
    );
  }
}

class Home extends StatefulWidget {
  Home({Key? key}) : super(key: key);

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  final List<String> gambar = [
    "img_saly10.png",
    "img_saly20.png",
    "img_saly24.png",
    "img_saly25.png"
  ];

  static const Map<String, Color> colors = {
    'img_saly10': Color(0xFF2DB569),
    'img_saly20': Color(0xFF386B8),
    'img_saly24': Color(0xFF45CAF5),
    'img_saly25': Color(0xFFB19ECB),
  };

  @override
  Widget build(BuildContext context) {
    return Container();
  }
}

class Pilihan {
  const Pilihan({
    required this.teks,
    required this.warna,
  });
  final String teks;
  final Color warna;
}

List<Pilihan> listPilihan = const <Pilihan>[
  const Pilihan(teks: "Red", warna: Colors.red),
  const Pilihan(teks: "Green", warna: Colors.green),
  const Pilihan(teks: "Blue", warna: Colors.blue),
];
